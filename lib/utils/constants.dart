import 'package:flutter/material.dart';

Color primaryBlue=Color(0xff3080ED);
Color secondaryOrange=Color(0xffFF7801);
String openSans="Open Sans";
String logoPath="assets/login_logo.png";
Color outlineGrey=Colors.grey;
Color transparent=Colors.transparent;
Color white=Colors.white;
Color red=Colors.red;
